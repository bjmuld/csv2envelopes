import csv
import sys
import os
import matplotlib.font_manager as fm
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

returnAddress = ['Ashley King and Barry Muldrey', '689 Berne Street SE, Apt C', 'Atlanta, GA  30312']
firstfields = ['Name', 'AddressLine1', 'AddressLine2', 'AddressLine3', 'City']

returnfont = fm.FontProperties(fname='fonts/Times New Roman.ttf', size=12)
returnskip = 0.05
returnx = -0.1
returny = 1.025

addfont = fm.FontProperties(fname='fonts/Times New Roman.ttf', size=16)
addskip = 0.075
addx = 0.225
addy = 0.4

infilearg = sys.argv[1]

if len(sys.argv)>2:
    outfilearg = sys.argv[2]
else:
    outfilearg = infilearg.split('.')[0] + '.pdf'

infile = infilearg
if not os.path.isfile(infile):
    if not infilearg.endswith('.csv'):
        infile = infilearg + '.csv'
        if not os.path.isfile(infile):
            print('couldn\'t find input csv file')
            exit(1)
    else:
        print('couldn\'t find input csv file')
        exit(1)

if not outfilearg.endswith('.pdf'):
    outfile = outfilearg+'.pdf'
else:
    outfile = outfilearg

outfilebase = outfile.split('.pdf')

ct = 1
while os.path.isfile(outfile):
    outfile = outfilebase[0] + str(ct) + '.pdf'
    ct += 1

with open(infile) as csvfile:
    dialect = csv.Sniffer().sniff(csvfile.read(1024))
    csvfile.seek(0)
    reader = csv.reader(csvfile, dialect)

    fieldnames = reader.__next__()
    csvfile.seek(0)
    dreader = csv.DictReader(csvfile, fieldnames=fieldnames, dialect=dialect)

    with PdfPages(outfile) as pdf:
        fig = plt.figure(figsize=(7.0, 5.0), dpi=150)

        for line in dreader:
            if (not line['Name']) or (line['Name'] == 'Name') or (not line['AddressLine1']) or\
                    (not line['City']) or (line['Printed']) or (not line['Proofed']):
                continue
            else:

                cursor = 0
                for retline in returnAddress:
                    plt.text(returnx, returny-cursor*returnskip, retline, ha='left', figure=fig,
                             fontproperties=returnfont)
                    cursor+=1

                # here come the addresses!!!!!!
                cursor = 0
                for key in firstfields:

                    if key == 'City':
                        plt.text(addx, addy - cursor * addskip, line['City']+', '+line['State']+'  '+line['ZIP'],
                                 ha='left', figure=fig, fontproperties=addfont)
                        cursor += 1
                    else:
                        if line[key]:
                            plt.text(addx, addy - cursor * addskip, line[key],
                                     ha='left', figure=fig, fontproperties=addfont)
                            cursor += 1

                plt.axis('off')
                pdf.savefig(fig)
                plt.clf()

        pgct = pdf.get_pagecount()

if pgct == 0:
    os.remove(outfile)

exit(0)
